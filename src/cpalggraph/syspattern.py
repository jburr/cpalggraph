"""Helper functions for manipulating container/aux names with systematics"""

from typing import Optional


systematic_key = "%SYS%"
nominal_name = "NOSYS"


def add_sys_pattern(name: str) -> str:
    """If the provided name doesn't contain the '%SYS%' pattern add it to the end"""
    return name if systematic_key in name else f"{name}_{systematic_key}"


def has_sys_pattern(name) -> bool:
    """Whether the provided name has the sys pattern"""
    return systematic_key in name


def replace_sys_pattern(name: str, replace_with: Optional[str] = None):
    """Replace the %SYS% pattern in the provided string

    In order to deal with the (usual) case where the pattern is added as a '_%SYS%'
    suffix and the pattern to be replaced with is an empty string, this whole suffix
    will just be removed. In order to prevent this, just supply an empty string to the
    replace_with parameter
    """
    if replace_with is None and name.endswith("_" + systematic_key):
        name = name[: -(len(systematic_key) + 1)]
    if replace_with is None:
        replace_with = ""
    return name.replace(systematic_key, replace_with)
