"""Helper dictionary that records the required preselections for each aux item"""

from collections import defaultdict
import collections.abc
from typing import Dict, Iterator, Union

import boolean

from cpalggraph.booleanalgebra import PRESELECTED_OBJECTS, algebra
from cpalggraph.containergraph import ContainerGraph
from cpalggraph.datatypes import Dependency, Selection


class PreselDict(collections.abc.Mapping[Dependency, boolean.Expression]):
    """Dictionary object that contains preselections for each aux item

    The dictionary is filled in reverse starting from the end of the job. As each node
    is passed it reads the preselections required by any data it produces and requests
    the necessary preselections for any data it requires.
    """

    def __init__(self, containers: ContainerGraph) -> None:
        """Create the dictionary

        Parameters
        ----------
        containers : ContainerGraph
            The relationships between containers and where auxdata is stored
        """
        self._containers = containers
        self._values: Dict[
            str, Dict[Union[str, None], boolean.Expression]
        ] = defaultdict(lambda: defaultdict(lambda: PRESELECTED_OBJECTS))

    def __getitem__(self, key: Dependency) -> boolean.Expression:
        return self._values[key.container][key.aux]

    def __iter__(self) -> Iterator[Dependency]:
        for container, dct in self._values.items():
            for aux in dct:
                yield Dependency(container, aux)

    def __len__(self) -> int:
        return sum(map(len, self._values.values()))

    def require(self, data: Dependency, expr: boolean.Expression) -> None:
        """Report a requirement on a data item

        Parameters
        ----------
        data : Dependency
            The data item
        expr : boolean.Expression
            The objects on which it is required
        """
        original, path = self._containers.find_producing_container(
            data, return_path=True
        )
        self._values[original][data.aux] |= expr
        for container in path:
            self._values[container][None] |= expr

    def make_unavailable(self, selection: Selection) -> None:
        """Tell the preselection dictionary that a selection is no longer available

        This will be called as the scheduler passes back past a node that produces a
        selection and is used to ensure that no preselection upstream includes this
        selection.

        If the selection has a preselection inherent in it (for example OR has the
        input label inherent) then the selection will be replaced with this
        preselection. Otherwise it will be removed completely.

        Parameters
        ----------
        selection : Selection
            The selection to remove
        """
        # Find all containers this could affect
        containers = self._containers.find_matching_dependencies(selection.dependency)
        symbol = algebra.Symbol(selection.dependency.aux)
        for container in containers:
            for k, v in self._values[container].items():
                if selection.preselection is None:
                    # Just remove the symbol
                    self._values[container][k] = algebra.remove_symbols(v, {symbol})
                else:
                    # Substitute in the preselection
                    self._values[container][k].subs(
                        {symbol: selection.preselection}, simplify=True
                    )

    def drop(self, data: Dependency) -> None:
        """Stop tracking an aux item"""
        self._values[data.container].pop(data.aux, None)
