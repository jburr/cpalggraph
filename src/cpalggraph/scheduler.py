from collections import defaultdict
import copy
import logging
from types import MappingProxyType
from typing import DefaultDict, Dict, List, Optional, Sequence, Set, Tuple, Union

import boolean
from componentwrapper.icreatable import ICreatable
import networkx as nx

from componentwrapper.jobconfiguration import JobConfiguration
from cpalggraph.basegraph import BaseGraph, EdgeTypeEnum
from cpalggraph.booleanalgebra import ALL_OBJECTS
from cpalggraph.configurationinfo import ConfigurationInfo
from cpalggraph.containergraph import ContainerGraph
from cpalggraph.datatypes import Dependency, Selection
from cpalggraph.graphbuilder import GraphBuilder
from cpalggraph.node import Node, auxdict_to_dependency_set
from cpalggraph.preseldict import PreselDict

log = logging.getLogger(__name__)


class InputNode(Node):
    """Node representing the input to the scheduler

    This is a dummy node (adds no algorithms) in the graph that tells the scheduler what
    input information is available
    """

    def __init__(self) -> None:
        #: The input information, batched by container
        self.input_aux: DefaultDict[str, Set[str]] = defaultdict(set)

    @property
    def name(self) -> str:
        return "__INPUT_NODE__"

    def produces(self) -> Set[Dependency]:
        return super().produces() | auxdict_to_dependency_set(self.input_aux)

    def requires(self) -> Set[Dependency]:
        return super().requires()

    def produces_containers(self) -> Dict[str, Optional[str]]:
        return {k: None for k in self.input_aux}

    def create_algs(self, info: ConfigurationInfo) -> List[ICreatable]:
        return []

    @property
    def has_job_output(self) -> bool:
        return False

    def __eq__(self, other) -> bool:
        return self is other

    def __hash__(self) -> int:
        return super().__hash__()


class OutputNode(Node):
    """Node representing the output of the scheduler

    This is a dummy node (adds no algorithms) in the graph that represents the output.
    It produces no data and has all the required output information as required data
    dependencies
    """

    def __init__(self):
        self.output_aux = DefaultDict[str, Set[str]](set)
        self.container_selections = DefaultDict[str, boolean.Expression](
            lambda: ALL_OBJECTS
        )
        self.output_selections = DefaultDict[Dependency, boolean.Expression](
            lambda: ALL_OBJECTS
        )

    @property
    def name(self) -> str:
        return "__OUTPUT_NODE__"

    def produces(self) -> Set[Dependency]:
        return super().produces()

    def produces_containers(self) -> Dict[str, Optional[str]]:
        return {}

    def requires(self) -> Set[Dependency]:
        return (
            auxdict_to_dependency_set(self.output_aux)
            | {
                Dependency(container, str(symbol))
                for container, expr in self.container_selections.items()
                for symbol in expr.symbols
            }
            | {
                Dependency(container, str(symbol))
                for container, expr in self.output_selections.items()
                for symbol in expr.symbols
            }
        )

    def create_algs(self, info: ConfigurationInfo) -> List[ICreatable]:
        return []

    def finalize(
        self,
        algorithms: Sequence[ICreatable],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        for aux in self.requires():
            required_output.require(
                aux,
                self.output_selections[aux]
                if aux in self.output_selections
                else self.container_selections[aux.container],
            )

    def __eq__(self, other) -> bool:
        return self is other

    def __hash__(self) -> int:
        return super().__hash__()

    @property
    def has_job_output(self) -> bool:
        return False


class Scheduler(GraphBuilder[Node, Dependency]):
    """Object responsible for collecting and ordering the nodes"""

    def __init__(self) -> None:
        super().__init__()
        #: The relations between containers and their auxdata
        self._containers = ContainerGraph()
        #: Selections defined by nodes in the job
        self._selections: Dict[Dependency, Selection] = {}
        #: Dummy node representing the inputs to the graph
        self._input_node = InputNode()
        #: Dummy node representing the outputs of the graph
        self._output_node = OutputNode()
        self += [self._input_node, self._output_node]

    @property
    def input_node(self) -> InputNode:
        """Dummy node representing the inputs to the graph"""
        return self._input_node

    @property
    def output_node(self) -> OutputNode:
        """Dummy node representing the outputs of the graph"""
        return self._output_node

    @property
    def container_graph(self) -> ContainerGraph:
        """The containers and their relationships"""
        return self._containers

    @property
    def selections(self) -> MappingProxyType[Dependency, Selection]:
        """The selections defined by nodes in the job"""
        return MappingProxyType(self._selections)

    def get_selection(self, dependency: Dependency) -> Selection:
        """Get the details about a selection

        Parameters
        ----------
        dep : Dependency
            The dependency associated to the selection

        Returns
        -------
        Selection
            The full selection

        Raises
        ------
        KeyError
            The dependency is unknown
        ValueError
            The dependency is not a Selection
        """
        true_container = self.container_graph.find_producing_container(dependency)
        try:
            return self._selections[Dependency(true_container, dependency.aux)]
        except KeyError:
            raise ValueError(dependency)

    def add_node(self, node: Node) -> bool:
        if not super().add_node(node):
            return False
        for container, parent in node.produces_containers().items():
            self.container_graph.add_container(container, parent)
        for dep in node.produces():
            self.container_graph.add_data(dep)
        return True

    def remove_node(self, node: Union[str, Node]):
        if not isinstance(node, Node):
            node = self[node]
        super().remove_node(node)
        for container, parent in node.produces_containers().items():
            if parent is not None:
                self.container_graph.remove_edge(container, parent)
        for dep in node.produces():
            self.container_graph.remove_data(dep)

    def get_producer(self, data: Dependency) -> Node:
        true_container = self.container_graph.find_producing_container(data)
        return super().get_producer(Dependency(true_container, data.aux))

    def build_graph(
        self,
        ignore_unmet: bool = True,
        prune_unused: bool = True,
        warn_unused: bool = True,
    ) -> BaseGraph[Node]:
        """Create the dependency graph

        Parameters
        ----------
        ignore_unmet: bool
            If False, raise a KeyError if a node depends on data which has no known
            producer
        prune_unused : bool, optional
            Remove nodes whose outputs are not used, by default True
        warn_unused : bool, optional
            Warn about unused nodes, by default True

        Returns
        -------
        The produced dependency graph

        Raises
        ------
        KeyError
            A node depends on data with no known producer and ignore_unmet is False
        CyclicGraphError
            The graph contains cycles
        """
        graph = super().build_graph(ignore_unmet)
        # Add edges to the output node for any node which provides a job output
        for node in self.values():
            if node.has_job_output:
                graph.add_edge(node, self.output_node)

        # Now look for any nodes that haven't been used
        used = set(nx.ancestors(graph, self.output_node))
        unused = set(self.values()) - used
        unused.discard(self.output_node)
        if warn_unused and unused:
            log.warning("Unused nodes have been detected in the graph:")
            for node in unused:
                log.warning("\t%s", node.name)
        if prune_unused:
            graph.remove_nodes_from(unused)
        else:
            # If we're not going to prune the unused edges, then add edges from them
            # to the output node. We will rely on all paths ending at the output
            # node
            for node in unused:
                if graph.out_degree(node) == 0:
                    # We only do this for the leaf nodes, i.e. those without a
                    # following node. Other nodes must necessarily eventually lead
                    # to a current leaf node (we have no cycles in the graph)
                    graph.add_edge(node, self._output_node)
        return graph

    def create_algorithms(
        self,
        node_order: Sequence[Node],
        inputs: Dict[str, str],
        outputs: Dict[str, str],
    ) -> Tuple[JobConfiguration, ConfigurationInfo]:
        """Create the algorithms in the specified node order

        Parameters
        ----------
        node_order : Sequence[Node]
            The ordered nodes
        inputs : dict[str, str]
            Mapping from input nickname to StoreGate name
        outputs : dict[str, str]
            Mapping from output nickname to StoreGate name, allowed to be missing the
            _%SYS% suffix

        Returns
        -------
        JobConfiguration
            The full job configuration which can be used to populate the job in the
            relevant backend
        ConfigurationInfo
            Object holding information on the configuration
        """
        info = ConfigurationInfo(
            self.container_graph, copy.copy(inputs), copy.copy(outputs)
        )
        # Now create the algorithms
        algorithms = [node.create_algs(info) for node in node_order]
        # Now go back through and finalize them (in reverse order)
        required_output = PreselDict(info.container_graph)

        for node, algs in zip(reversed(node_order), reversed(algorithms)):
            node.finalize(algs, required_output, info)

        schedule = JobConfiguration()
        schedule += algorithms
        return schedule, info

    def _add_node_data_dependency(
        self, graph: BaseGraph[Node], node: Node, data: Dependency
    ) -> bool:
        # Find the original container
        try:
            original, path = self.container_graph.find_producing_container(
                data, return_path=True
            )
        except KeyError:
            return False
        stack = (
            [self.get_producer(Dependency(original, data.aux))]
            + [self.get_producer(Dependency(container, None)) for container in path[1:]]
            + [node]
        )

        for source, target, container in zip(stack, stack[1:], path):
            # Very rarely the source and target are the same. This can happen, e.g. when
            # a node creates some auxdata *and* a new container
            if source is target:
                continue
            graph.add_edge(source, target)
            graph.edges[source, target]["DataDependencies"].add(
                Dependency(container, data.aux)
            )
            graph.edges[source, target]["EdgeType"] = EdgeTypeEnum.Data
        return True
