"""Class responsible for constructing the basic dependency graph"""

from __future__ import annotations
import logging
from typing import Any, Callable, Generic, Iterable, Iterator, Mapping, TypeVar
import collections.abc

import networkx as nx

from cpalggraph.basegraph import N, BaseGraph, EdgeTypeEnum
from cpalggraph.basenode import DataT, BaseNode

SelfT = TypeVar("SelfT", bound="GraphBuilder")

log = logging.getLogger(__name__)


class GraphBuilder(collections.abc.Mapping[str, N], Generic[N, DataT]):
    """Base class for building the dependency graph

    This class holds a list of the nodes in a graph. The full graph is then created by
    the build_graph method. A full linear ordering obeying any weak dependencies and
    node priorities can be extracted using the order_graph method.

    This class also acts as a mapping from node names to the nodes themselves.
    """

    def __init__(self) -> None:
        """Create the builder"""
        #: Keep track of all the nodes we have
        self._nodes: list[N] = []
        #: For convenience, keep a mapping from produced information back to the node
        # producing it
        self._by_produced: dict[DataT, N] = {}
        #: Strong dependencies are enforced by the graph building and provide a way to
        # force algorithms into a certain order
        self._strong_deps: list[tuple[N, N]] = []
        #: Weak dependencies are used during the graph ordering and provide a way to
        # order algorithms only if that ordering doesn't break the depency ordering
        self._weak_deps: list[tuple[N, N]] = []

    def build_graph(self, ignore_unmet: bool = True) -> BaseGraph[N]:
        """Create the dependency graph

        Parameters
        ----------
        ignore_unmet: bool
            If False, raise a KeyError if a node depends on data which has no known
            producer

        Returns
        -------
        The produced dependency graph

        Raises
        ------
        KeyError
            A node depends on data with no known producer and ignore_unmet is False
        CyclicGraphError
            The graph contains cycles
        """
        graph = self._create_empty_graph()
        self._populate_graph(graph, ignore_unmet=ignore_unmet)
        for edge in self._weak_deps:
            graph.add_edge(*edge, EdgeType=EdgeTypeEnum.Weak)
        for edge in self._strong_deps:
            graph.add_edge(*edge, EdgeType=EdgeTypeEnum.Strong)
        return graph.generate_acyclic_graph()

    def order_graph(self, graph: BaseGraph[N]) -> list[N]:
        """Calculate the execution order for the graph

        The graph will be ordered using the topological sort algorithm. Ties are broken
        using the function returned by _make_node_key_func

        Parameters
        ----------
        graph: BaseGraph
            The graph to order (should be returned by the build_graph method)

        Returns
        -------
        A list of the nodes in the chosen execution order
        """
        return list(
            nx.algorithms.dag.lexicographical_topological_sort(
                graph, self.get_topological_key()
            )
        )

    @classmethod
    def annotate_order(
        cls, graph: BaseGraph[N], order: list[N], key: str = "TotalOrder"
    ):
        """Annotate edges in the graph with an attribute saying if it is in the order

        Parameters
        ----------
        graph: BaseGraph
            The graph to annotate
        order: list[BaseNode]
            The ordering of the nodes to annotate. Should be returned by the
            ``order_graph`` method
        key: str
            The name of the edge attribute under which to store the ordering

        The attribute value will be True for an edge in the order and False otherwise.
        """
        graph.define_edge_attribute(key, False)
        nx.add_path(graph, order, **{key: True, "EdgeType": EdgeTypeEnum.Order})

    def get_producer(self, data: DataT) -> N:
        """Get the node which produces a particular data item

        Parameters
        ----------
        data : DataT
            The data being queried

        Raises
        ------
        KeyError: No node produces this data
        """
        return self._by_produced[data]

    def add_node(self, node: N) -> bool:
        """Add a new node into the node list

        If the node already exists this is a no-op

        Raises
        ------
        KeyError
            There is a node with the same name that does not compare equal
        ValueError
            There is already a node which produces one of this node's data items

        Returns
        -------
        bool
            Whether the node was added
        """
        log.debug("Adding node '%s", node.name)

        try:
            existing = self[node.name]
        except KeyError:
            # No node has that name, keep going
            pass
        else:
            if existing == node:
                # All good, this is just the same node already in the graph
                log.debug("Node already exists in the graph")
                return False
            else:
                raise KeyError(node.name)

        for data in node.produces():
            try:
                existing = self.get_producer(data)
            except KeyError:
                pass
            else:
                raise ValueError(data)

        self._nodes.append(node)
        for data in node.produces():
            self._by_produced[data] = node
        return True

    def remove_node(self, node: str | N):
        """Remove a node from the graph

        Parameters
        ----------
        node : str | BaseNode
            The node to remove, can be the node object or its name

        Raises
        ------
        KeyError
            The node does not exist in the graph builder
        """
        if isinstance(node, str):
            node = self[node]
        try:
            idx = self._nodes.index(node)
        except ValueError:
            raise KeyError(node)

        del self._nodes[idx]
        for data in node.produces():
            del self._by_produced[data]

    def add_weak_dependency(self, source: str | N, target: str | N):
        """Add a weak dependency to the graph

        A weak dependency is an ordering hint to the scheduler and will be ignored if
        it would cause a cycle

        Parameters
        ----------
        source: str, BaseNode
            The node which should come first
        target: str, BaseNode
            The node which should come second

        Raises
        ------
        KeyError
            One of the nodes is not in the graph
        """
        if isinstance(source, str):
            source = self[source]
        elif source not in self:
            raise KeyError(source.name)
        if isinstance(target, str):
            target = self[target]
        elif target not in self:
            raise KeyError(target.name)
        self._weak_deps.append((source, target))

    def add_strong_dependency(self, source: str | N, target: str | N):
        """Add a strong dependency to the graph

        A strong dependency enforces the order onto the graph and will cause an error
        if it introduces a cycle.

        Parameters
        ----------
        source: str, BaseNode
            The node which will come first
        target: str, BaseNode
            The node which will come second

        Raises
        ------
        KeyError
            One of the nodes is not in the graph
        """
        if isinstance(source, str):
            source = self[source]
        elif source not in self:
            raise KeyError(source.name)
        if isinstance(target, str):
            target = self[target]
        elif target not in self:
            raise KeyError(target.name)
        self._strong_deps.append((source, target))

    def get_topological_key(self) -> Callable[[N], Any]:
        """The default function to use when ordering nodes

        Returns a function that orders each node first by priority and then by the
        insertion order
        """
        # NB: negate the priority so that higher priorities come first
        return lambda node: (-node.priority, self._nodes.index(node))

    def _create_empty_graph(self) -> BaseGraph[N]:
        return BaseGraph.graph_factory()

    def _populate_graph(self, graph: BaseGraph[N], ignore_unmet: bool):
        for node in self.values():
            graph.add_node(node, label=node.name)
            log.debug(
                "Add node %s with requirements %s to the graph", node, node.requires()
            )
            for data in node.requires():
                if (
                    not self._add_node_data_dependency(graph, node, data)
                    and not ignore_unmet
                ):
                    # _add_node_data_dependency returns False if the dependency is not
                    # met
                    raise ValueError(data)

    def _add_node_data_dependency(
        self, graph: BaseGraph[N], node: N, data: DataT
    ) -> bool:
        """Add an edge to the graph representing a node's data dependency

        Parameters
        ----------
        graph : BaseGraph
            The graph to add the edge to
        node : BaseNode
            The node whose data dependency is being processed
        data : Data
            The data on which the node depends

        Returns
        -------
        bool
            Whether the new edge was added
        """
        try:
            producer = self.get_producer(data)
        except KeyError:
            log.debug("Dependency '%s' of node '%s' is not satisfied", data, node)
            return False
        graph.add_edge(producer, node, DataDependencies={data})
        return True

    def __getitem__(self, name: str) -> N:
        try:
            return next(n for n in self._nodes if n.name == name)
        except StopIteration:
            raise KeyError(name)

    def __delitem__(self, name: str):
        """Remove a node by name"""
        self.remove_node(name)

    def __iter__(self) -> Iterator[str]:
        return iter(n.name for n in self._nodes)

    def __len__(self) -> int:
        return len(self._nodes)

    def __iadd__(self: SelfT, value: N | Mapping[Any, N] | Iterable) -> SelfT:
        """Add nodes through the += operator"""
        if isinstance(value, BaseNode):
            self.add_node(value)
        elif isinstance(value, collections.abc.Mapping):
            for x in value.values():
                self += x  # type: ignore
        elif isinstance(value, collections.abc.Iterable):
            for x in value:
                self += x  # type: ignore
        else:
            raise TypeError(type(value))
        return self
