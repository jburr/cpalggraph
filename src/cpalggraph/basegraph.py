"""Base class for graph representations"""

from __future__ import annotations
import enum
from typing import Any, Callable, Dict, Generic, Tuple, TypeVar

import networkx as nx

from cpalggraph.basenode import BaseNode

N = TypeVar("N", bound=BaseNode)
SelfT = TypeVar("SelfT", bound="BaseGraph")


class CyclicGraphError(ValueError):
    """Error raised when the produced graph would contain a cycle"""

    def __init__(self, cycles: Tuple[Tuple[BaseNode, ...], ...]):
        """Create the exception

        Parameters
        ----------
        cycles : list[list[BaseNode]]
            The list of cycles
        """
        self.cycles = cycles
        super().__init__(
            "Cycles detected: {}".format(
                [
                    # Make the list actually look like a cycle by repeating the
                    # starting node
                    " -> ".join([c.name for c in cycle] + [cycle[0].name])
                    for cycle in cycles
                ]
            )
        )


class EdgeTypeEnum(enum.Enum):
    """Enum describing the different edge types in the graph

    Most edges will be data dependencies but additional edges may be added to enforce
    orderings not easily expressed by dependencies or to suggest a preferred ordering to
    the scheduler.

    Where an edge is in the graph for multiple reasons its most important (lowest
    valued) reason is used.
    """

    #: The edge shows a data dependency
    Data = 0
    #: Not a data dependency but still must be obeyed
    Strong = 1
    #: Allowed to influence the ordering unless it would introduce a cycle
    Weak = 2
    #: This edge is added as part of the execution order
    Order = 3


class BaseGraph(nx.DiGraph, Generic[N]):
    """Base for custom graph classes

    The edges in this graph represent data dependencies between the nodes. The
    individual data items are then added to these edges as attributes. Additional edges
    can be added to further influence the ordering
    """

    @classmethod
    def edge_attr_dict_factory(cls) -> Dict[str, Any]:
        """Define attributes to appear on every edge"""
        return {"DataDependencies": set(), "EdgeType": EdgeTypeEnum.Strong}

    @classmethod
    def graph_factory(cls) -> BaseGraph[N]:
        """Return an empty graph"""
        return cls()

    def add_edge(
        self,
        u_of_edge: N,
        v_of_edge: N,
        DataDependencies: set | None = None,
        EdgeType=EdgeTypeEnum.Strong,
        **attr,
    ):
        # Override add_edge to ensure that data dependencies and edge type are always
        # correctly set
        if DataDependencies:
            # If there are data dependencies the edge type *must* be 'Data'
            EdgeType = EdgeTypeEnum.Data
        elif DataDependencies is None:
            DataDependencies = set()
        if self.has_edge(u_of_edge, v_of_edge):
            attrs = self.get_edge_data(u_of_edge, v_of_edge)
            attrs["DataDependencies"] |= DataDependencies
            attrs["EdgeType"] = min(EdgeType, attrs["EdgeType"], lambda e: e.value)
        else:
            super().add_edge(
                u_of_edge,
                v_of_edge,
                DataDependencies=DataDependencies,
                EdgeType=EdgeType,
                **attr,
            )

    def define_edge_attribute(self, key: str, default_value: Any):
        """Define a new attribute

        Parameters
        ----------
        key: str
            The name of the attribute
        default_value
            The default value to set for the attribute. It can also be a callable,
            taking the in node, out node and edge as arguments and returning the new
            value to set.
        """
        for in_node, out_node, edge in self.edges(data=True):
            if callable(default_value):
                edge[key] = default_value(in_node, out_node, edge)
            else:
                edge[key] = default_value

    def filter_edges(self, condition: Callable[[N, N, dict], bool]) -> BaseGraph[N]:
        """Produces a subgraph containing only edges passing the condition

        Parameters
        ----------
        condition: Callable[[BaseNode, BaseNode, dict], bool]
            The returned subgraph will only contain edges which pass this condition.
            The arguments are the input and output nodes of the edge and its dictionary
            of attributes.
        """
        # Create the empty graph
        graph = self.graph_factory()
        # Add only those edges which satisfy the condition
        graph.add_edges_from(e for e in self.edges(data=True) if condition(*e))
        # Add back all the node attributes, only for those nodes in the new graph
        nx.set_node_attributes(graph, {node: self.nodes[node] for node in graph})
        return graph

    def data_dependency_graph(self) -> BaseGraph[N]:
        """Return a graph only containing the data dependencies"""
        # NB: The edge attributes are stored in the third member of the tuple returned
        # by edges
        return self.filter_edges(
            lambda _1, _2, attrs: attrs["EdgeType"] == EdgeTypeEnum.Data
        )

    def strong_dependency_graph(self) -> BaseGraph[N]:
        """Return a graph containing all strong dependencies

        This includes the data dependencies"""
        return self.filter_edges(
            lambda _1, _2, attrs: attrs["EdgeType"]
            in (EdgeTypeEnum.Data, EdgeTypeEnum.Strong)
        )

    def generate_acyclic_graph(self: SelfT) -> SelfT:
        """Generate an acyclic graph

        If there are no cycles, merely returns a copy of this. Otherwise it will remove
        weak edges until there are no cycles.

        Raises
        ------
        CyclicGraphError
            There is at least one cycle containing no weak edges
        """
        copy = self.filter_edges(lambda _1, _2, _3: True)
        cycles: tuple[list[N], ...] = tuple(nx.algorithms.cycles.simple_cycles(copy))
        # Keep track of all edges we have already removed to try and remove a reasonably
        # minimal set - finding *the* minimal set is likely to be NP-complete...
        removed: set[tuple[N, N]] = set()
        # Keep track of all 'true' cycles, i.e. those that cannot be resolved so that we
        # can report a full error
        true_cycles: list[tuple[N, ...]] = []
        for cycle in cycles:
            for edge in zip(cycle, cycle[1:]):
                if edge in removed:
                    # If we've already removed the edge then this cycle has been broken
                    break
                if copy.get_edge_data(*edge)["EdgeType"] == EdgeTypeEnum.Weak:
                    removed.add(edge)
                    copy.remove_edge(*edge)
                    break
            else:
                # No weak edge was encountered => this is a 'true' cycle
                true_cycles.append(tuple(cycle))
        if true_cycles:
            raise CyclicGraphError(tuple(true_cycles))
        return copy
