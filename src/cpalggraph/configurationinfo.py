"""Class holding information about the job configuration

This object is created by the scheduler using information from the graph.

After the execution order is determined by the dependency mechanism the scheduler
performs two passes through the algorithms. The first is in the order of execution and
the ConfigurationInfo is built during this step. The second is back in reverse order.
The ConfigurationInfo object is then used to finalize the node configurations
"""

from typing import DefaultDict, Dict, List, Optional, Tuple, Union, overload

import boolean

from cpalggraph.booleanalgebra import algebra
from cpalggraph.containergraph import ContainerGraph
from cpalggraph.datatypes import Dependency, Selection
from cpalggraph.syspattern import add_sys_pattern, has_sys_pattern


def _get_temporary_name(base: str, count: int) -> str:
    """Get a temporary container name

    Parameters
    ----------
    base : str
        The base for the temporary name
    count : int
        The number of temporary containers already made
    """
    return f"{base}_TMP{count}"


class ConfigurationInfo:
    """Class holding information about the full job configuration

    Used to pass information between the nodes

    The class calculates the following information:

    container relations:
        This information is stored in a container graph object

    container names:
        Tracks current container names to ensure to correctly handle temporary copies

    auxdata:
        A mapping from dependencies to the 'true' aux name in the few cases where this
        is different

    selections:
        Keep track of the order in which selections are defined. This is to ensure the
        correct preselections can be calculated for AND expressions
    """

    def __init__(
        self,
        containers: ContainerGraph,
        inputs: Dict[str, str],
        outputs: Dict[str, str],
    ) -> None:
        """Create the object

        Parameters
        ----------
        containers : ContainerGraph
            The container relations
        inputs : dict[str, str]
            Mapping from an input nickname to the actual name in StoreGate
        outputs : dict[str, str]
            Mapping from an output nickname to the actual name in StoreGate
        """
        self._containers = containers
        self._inputs = inputs
        self._outputs = outputs

        #: 'True' auxdata names where these differ from their nicknames
        self._auxdata: Dict[Dependency, str] = {}
        #: Record the selections used in the job
        self._selections: Dict[Dependency, Selection] = {}
        #: Selections in the order in which they are defined
        self._selection_order: Dict[Dependency, int] = {}
        #: The names of containers
        self._container_name_stack = DefaultDict[str, List[str]](list)

    @property
    def container_graph(self) -> ContainerGraph:
        """The container graph"""
        return self._containers

    def get_current_container_name(self, nickname: str) -> str:
        """Get the current name of a container"""
        stack = self._container_name_stack[nickname]
        return stack[-1] if stack else self._inputs.setdefault(nickname, nickname)

    def get_next_container_name(self, nickname: str, is_sys_varied: bool = True) -> str:
        """Get the next version of a container's name

        Will create a temporary copy if necessary

        Parameters
        ----------
        nickname : str
            The nickname of the container
        is_sys_varied : bool, optional
            Whether the new container is systematically varied, by default True. If the
            current container version is systematically varied the new container is
            forced to be

        Returns
        -------
        str
            The next container version
        """
        stack = self._container_name_stack[nickname]
        n_used = len(stack)
        if stack and has_sys_pattern(stack[-1]):
            is_sys_varied = True
        # Figure out the base name. Pick the first available of an associated output,
        # associated output or the nickname itself
        if nickname in self._outputs:
            base = self._outputs[nickname]
        elif nickname in self._inputs:
            base = self._inputs[nickname]
        else:
            base = nickname
        next_name = _get_temporary_name(base, n_used)
        if is_sys_varied:
            next_name = add_sys_pattern(next_name)
        stack.append(next_name)
        return next_name

    def replace_final_container_name(self, nickname: str, current_value: str) -> str:
        """Replace the final temporary container name with the correct output

        Parameters
        ----------
        nickname : str
            The container nickname
        current_value : str
            The current value of the container name on the node under consideration

        Returns
        -------
        str
            The correct name
        """
        if current_value == self.get_current_container_name(nickname):
            try:
                return self._outputs[nickname]
            except KeyError:
                if nickname in self._inputs:
                    # We've already used this as an input, can't use it as an output
                    return current_value
                else:
                    return (
                        add_sys_pattern(nickname)
                        if has_sys_pattern(current_value)
                        else nickname
                    )
        else:
            return current_value

    def set_true_auxdata(self, dep: Dependency, true_aux: str) -> None:
        """Set the 'true' name of a piece of auxdata

        This should only be used when this differs from the value in the dependency
        """
        self._auxdata[dep] = true_aux

    def get_true_auxdata(self, dep: Dependency) -> str:
        """Get the 'true' name of a piece of auxdata

        Raises
        ------
        ValueError
            The depenency is on a container
        """
        if dep.aux is None:
            raise ValueError(dep)
        true_container = self.container_graph.find_producing_container(dep)
        return self._auxdata.get(Dependency(true_container, dep.aux), dep.aux)

    def add_selection(self, selection: Selection) -> None:
        """Tell this object about a new selection"""
        if selection.is_sys_varied:
            assert selection.dependency.aux
            self.set_true_auxdata(
                selection.dependency, add_sys_pattern(selection.dependency.aux)
            )
        self._selection_order[selection.dependency] = len(self._selection_order)
        self._selections[selection.dependency] = selection

    def get_selection(self, dep: Dependency) -> Selection:
        """Get the details about a selection

        Parameters
        ----------
        dep : Dependency
            The dependency associated to the selection

        Returns
        -------
        Selection
            The full selection

        Raises
        ------
        KeyError
            The dependency is unknown
        """
        true_container = self.container_graph.find_producing_container(dep)
        return self._selections[Dependency(true_container, dep.aux)]

    @overload
    def get_selection_property_value(self, dep: Dependency, /) -> str:
        ...

    @overload
    def get_selection_property_value(
        self,
        container: str,
        expression: boolean.Expression,
        /,
    ) -> str:
        ...

    def get_selection_property_value(
        self,
        dep_or_container: Union[str, Dependency],
        expression: Optional[boolean.Expression] = None,
        /,
    ) -> str:
        """Get the correct string for a property

        Can be called either with a single dependency or a container + expression. The
        second should be used when setting an input selection property

        Parameters
        ----------
        dep_or_container : str | Dependency
            Either the container name or a full dependency
        expression : boolean.Expression | None
            The expression

        Returns
        -------
        str
            The value to be set as a property
        """
        if isinstance(dep_or_container, Dependency):
            selection = self.get_selection(dep_or_container)
            trueaux = self.get_true_auxdata(dep_or_container)
            return f"{trueaux},{'as_char' if selection.counts is None else 'as_bits'}"
        else:
            assert expression is not None
            return str(
                expression.simplify().subs(
                    {
                        symbol: algebra.Symbol(
                            self.get_selection_property_value(
                                Dependency(dep_or_container, str(symbol))
                            )
                        )
                        for symbol in expression.symbols
                    }
                )
            )

    def get_symbol_order(
        self, container: str, expr: boolean.AND
    ) -> Tuple[boolean.Expression, ...]:
        """Get the order in which the selections of an AND are fully available"""
        order: List[Tuple[int, boolean.Expression]] = []
        for subexpr in expr.args:
            # Get the highest index among this sub-expression's symbols
            idx = max(
                (
                    self._selection_order.get(
                        self.get_selection(
                            Dependency(container, str(symbol))
                        ).dependency,
                        -1,
                    )
                    for symbol in subexpr.symbols
                ),
                default=0,
            )
            order.append((idx, subexpr))
        # Now return them sorted on their index
        return tuple(subexpr for _, subexpr in sorted(order))
