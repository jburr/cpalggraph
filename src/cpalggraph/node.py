"""Implementation of the main Node class

A node represents the smallest unit of the job configuration that the scheduler is
allowed to reorder
"""

import abc
import logging
from typing import Dict, Iterable, List, Mapping, Optional, Sequence, Set, Tuple, Union
from typing_extensions import TypeAlias

from componentwrapper.icreatable import ICreatable

from cpalggraph.basenode import BaseNode
from cpalggraph.configurationinfo import ConfigurationInfo
from cpalggraph.datatypes import Dependency, Selection
from cpalggraph.preseldict import PreselDict

log = logging.getLogger(__name__)


def auxdict_to_dependency_set(auxdict: Mapping[str, Iterable[str]]) -> Set[Dependency]:
    """Convert a mapping from container -> list of aux items to a set of dependencies"""
    return {
        Dependency(container, aux)
        for container, auxitems in auxdict.items()
        for aux in auxitems
    } | {Dependency(container, None) for container in auxdict}


def merge_produces_containers(
    inputs: Iterable[Dict[str, Optional[str]]]
) -> Dict[str, Optional[str]]:
    """Merge 'produces_containers' dictionaries"""
    produces: Dict[str, Optional[str]] = {}
    for dct in inputs:
        for container, parent in dct.items():
            try:
                existing = produces[container]
            except KeyError:
                produces[container] = parent
            else:
                if existing != parent:
                    log.error(
                        f"Container {container} has different parents reported "
                        f"({existing} and {parent})"
                    )
                    raise ValueError(container)
    return produces


class Node(BaseNode[Dependency]):
    """Smallest unit of the job configuration

    Data dependencies are represented by Dependency objects, which are essentially
    2-tuples holding the container and auxdata names, where the auxdata name is None if
    the dependency is on the container alone.
    """

    def produces_selections(self) -> List[Selection]:
        """Any selections created by this node"""
        return []

    @abc.abstractmethod
    def produces(self) -> Set[Dependency]:
        return {sel.dependency for sel in self.produces_selections()}

    @abc.abstractmethod
    def requires(self) -> Set[Dependency]:
        return set()

    @abc.abstractmethod
    def produces_containers(self) -> Dict[str, Optional[str]]:
        """Any containers created by this node

        Containers are returned as a dictionary mapping from the created container
        nickname to its parent container (or None otherwise). A container has a parent
        if it is a deep copy, shallow copy or view. Where a node updates a container
        (i.e. creates a temporary copy) it should have itself as the parent.
        """
        return {}

    @abc.abstractmethod
    def create_algs(self, info: ConfigurationInfo) -> List[ICreatable]:
        """Create the algorithm(s) represented by this node

        This is called on the first pass through the ordered nodes and creates the
        actual algorithms.

        Parameters
        ----------
        info : ConfigurationInfo
            Object giving information about container relations, selections and scale
            factors. The node should update this with information about any that it
            creates

        Returns
        -------
        List[ICreatable]
            The created algorithms
        """

    def finalize(
        self,
        algorithms: Sequence[ICreatable],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        """Finalize the configuration of the created algorithms

        This is called as the scheduler passes back through the node order in
        *reverse* order and finalizes the configuration of the algorithms. For the
        most part this means setting preselections correctly and converting temporary
        container names to the expected final output names where relevant.

        This must update the required_output dictionary with two pieces of
        information:
            - Update any auxitems that this relies on upstream
            - Remove any selections this produces from the selections

        It's also good practice to remove any keys that this produces

        Parameters
        ----------
        algorithms : List[ICreatable]
            The algorithms returned by *this* node's create method
        required_output : dict[AuxData, boolean.Expression]
            Which objects are required
        """

    @abc.abstractproperty
    def has_job_output(self) -> bool:
        """Whether or not this node is responsible for producing output from the job.

        If a node produces output (e.g. a TTree or histogram) it is always required and
        so the scheduler will add a dependency from it to the output node.
        """


class CompositeNode(Node):
    """A single node formed by merging multiple other nodes

    This should be used when you want to force a group of nodes to all run in sequence
    """

    def __init__(self, name: str, nodes: Iterable[Node] = ()):
        self._name = name
        self._nodes = list(nodes)

    @property
    def name(self) -> str:
        return self._name

    @property
    def nodes(self) -> Tuple[Node, ...]:
        return tuple(self._nodes)

    def produces_containers(self) -> Dict[str, Optional[str]]:
        return merge_produces_containers(
            node.produces_containers() for node in self.nodes
        )

    def produces(self) -> Set[Dependency]:
        return set().union(*(node.produces() for node in self.nodes))

    def requires(self) -> Set[Dependency]:
        # Exclude any requirements produced by another node
        return set().union(*(node.requires() for node in self.nodes)) - self.produces()

    @property
    def has_job_output(self) -> bool:
        return any(node.has_job_output for node in self.nodes)

    def create_algs(self, info: ConfigurationInfo) -> List[ICreatable]:
        return sum((node.create_algs(info) for node in self.nodes), [])

    def finalize(
        self,
        algorithms: Sequence[ICreatable],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ):
        # Nodes must be finalized in *reverse* order
        for node, algorithm in zip(reversed(self.nodes), reversed(algorithms)):
            node.finalize([algorithm], required_output, info)

    def __eq__(self, other):
        return isinstance(other, CompositeNode) and self.nodes == other.nodes

    def __hash__(self) -> int:
        return hash((self.name, self.nodes))
