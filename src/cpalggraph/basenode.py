"""The base node class for the simple dependency graph"""

from __future__ import annotations
import abc
from typing import Generic, Hashable, Iterable, TypeVar

#: TypeVar for the data type used in dependencies
DataT = TypeVar("DataT", bound=Hashable)


class BaseNode(abc.ABC, Generic[DataT]):
    """Abstract base class for nodes in the dependency graph

    A node is the base unit of the graph. Each node the data that it produces and that
    it consumes. The dependency mechanism then uses this information to calculate the
    order in which they should be executed.

    Each node should have a unique name so that it can be retrieved from the graph.

    The extra priority property is used to break ties between nodes in the graph when
    deciding on the order. Nodes with higher priority will try to be placed before nodes
    with lower priority, though this will never cause any strong dependency to be
    violated.
    """

    @abc.abstractproperty
    def name(self) -> str:
        """The name of this node (should be unique within the graph)"""
        pass

    def __hash__(self) -> int:
        return hash(self.name)

    def __str__(self):
        """Provide a default string representation"""
        return self.name

    @abc.abstractmethod
    def __eq__(self, other):
        """Nodes must define equality"""

    @abc.abstractmethod
    def produces(self) -> Iterable[DataT]:
        """All data that this node produces"""

    @abc.abstractmethod
    def requires(self) -> Iterable[DataT]:
        """All data that thiis node requires"""

    @property
    def priority(self) -> int:
        """The graph builder will by default try to place high priority nodes early"""
        return 0
