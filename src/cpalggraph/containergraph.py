"""Class holding data on container relations and their associated auxdata"""

from typing import Any, Dict, List, Literal, Optional, Tuple, Union, cast, overload
import networkx as nx

from cpalggraph.datatypes import Dependency


class ContainerGraph:
    """Describes container relations and their auxdata"""

    class _Graph(nx.DiGraph):
        @classmethod
        def node_attr_dict_factory(cls) -> Dict[str, Any]:
            return {"auxdata": set()}

    def __init__(self) -> None:
        #: The actual container graph
        self._graph = self._Graph()

    def add_container(self, container: str, parent: Optional[str]) -> None:
        """Add a new container

        Parameters
        ----------
        container : str
            The nickname of the container
        parent : Optional[str]
            The nickname of the parent (if any)

        Raises
        ------
        ValueError
            The container already exists in the graph with a different parent
        """
        # First make sure the container doesn't already exist with a different parent
        existing_parent = self.get_parent(container)
        if existing_parent is None:
            # No parent, this is fine
            if parent is not None:
                # Make sure this would not induce a cycle
                if self.is_ancestor_of(container, parent):
                    raise ValueError(
                        f"Adding '{parent}' as the parent of '{container}' would "
                        "create a cycle"
                    )
                self._graph.add_edge(container, parent)
            else:
                self._graph.add_node(container)
        else:
            if parent != existing_parent:
                raise ValueError(
                    f"Different parents declared for container '{container}': "
                    f"'{parent}' != '{existing_parent}'"
                )

    def remove_edge(self, container: str, parent: str) -> None:
        """Remove the edge"""
        self._graph.remove_edge(container, parent)
        for c in container, parent:
            if nx.degree(self._graph, c) == 0 and not self._graph[c]["auxdata"]:
                self._graph.remove_node(c)

    def is_ancestor_of(self, ancestor: str, descendant: str) -> bool:
        """Check if one container is an ancestor of another

        This will not count a container as its own ancestor

        Parameter
        ---------
        ancestor : str
            The candidate ancestor
        descendant : str
            The candidate descendant
        """
        if ancestor not in self._graph or descendant not in self._graph:
            return False
        return ancestor != descendant and nx.has_path(self._graph, descendant, ancestor)

    def get_parent(self, container: str) -> Optional[str]:
        """Get the parent of a container or None if it has none"""
        in_edges = cast(List[Tuple[str, str]], self._graph.in_edges(container))
        assert len(in_edges) < 2
        if in_edges:
            return in_edges[0][1]
        return None

    def ancestors(self, container: str) -> Tuple[str, ...]:
        """Get the ancestors of the provided container

        The returned tuple will not include the container itself
        """
        ancestors: List[str] = []
        while container := self.get_parent(container):  # type: ignore
            ancestors.append(container)
        return tuple(ancestors)

    def add_data(self, data: Dependency):
        """Record produced data to the graph

        Parameters
        ----------
        data : Dependency
            The name of the produced data
        """
        if data.container not in self._graph:
            self._graph.add_node(data.container)
        self._graph.nodes[data.container]["auxdata"].add(data.aux)

    def remove_data(self, data: Dependency):
        """Remove data from the graph"""
        self._graph[data.container]["auxdata"].remove(data.aux)
        if (
            nx.degree(self._graph, data.container) == 0
            and not self._graph[data.container]
        ):
            self._graph.remove_node(data.container)

    @overload
    def find_producing_container(
        self, data: Dependency, return_path: Literal[False] = False
    ) -> str:
        ...

    @overload
    def find_producing_container(
        self, data: Dependency, return_path: Literal[True]
    ) -> Tuple[str, Tuple[str, ...]]:
        ...

    def find_producing_container(
        self, data: Dependency, return_path: bool = False
    ) -> Union[str, Tuple[str, Tuple[str, ...]]]:
        """Find the 'true' name of a piece of auxdata

        Parameters
        ----------
        data : Dependency
            The requested dependency
        return_path : bool
            If True, return the path from the requested container to the actual
            container in the graph.

        Returns
        -------
        Dependency
            The actual container on which the auxdata was defined

        Raises
        ------
        KeyError
            If the auxdata is not known
        """
        path = [data.container]
        if data.aux is None:
            # Weird thing to ask for but the container is always the same
            return (data.container, tuple(path)) if return_path else data.container
        while True:
            if data.aux in self._graph[path[-1]]:
                return (path[-1], tuple(path)) if return_path else path[-1]
            else:
                parent = self.get_parent(path[-1])
                if parent is None:
                    raise KeyError(data)
                path.append(parent)

    def find_matching_dependencies(self, data: Dependency) -> Tuple[str, ...]:
        """Find all dependencies matched by this data

        There is a many-to-one mapping of dependency names to auxdata. This method
        is the reverse of the find_producing_container method. For example, consider a
        container hierarchy

        baseElectrons -> calibElectrons -> finalElectrons

        where there is an auxitem 'var' which is produced on both 'baseElectrons' and
        'finalElectrons'. The dependency calibElectrons.var refers to the 'real'
        auxdata baseElectrons.var. Therefore
        get_matching_dependencies(baseElectrons.var) would return
        (baseElectrons, calibElectrons)

        Raises
        ------
        KeyError
            The provided dependency doesn't exist
        """
        try:
            self._graph[data.container][data.aux]
        except KeyError:
            raise KeyError(data)
        matching = [data.container]
        while current := self.get_parent(matching[-1]):
            if data.aux in self._graph[current]:
                break
            matching.append(current)
        return tuple(matching)
