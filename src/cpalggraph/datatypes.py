"""Data types used throughout the package"""

from dataclasses import astuple, dataclass
from typing import Optional

import boolean


@dataclass(frozen=True)
class Dependency:
    """An edge in the dependency graph"""

    #: The name of the container on which the data is defined
    container: str
    #: The name of the auxdata set. None if the dependency is just on the container
    aux: Optional[str]

    def __iter__(self):
        """Allow unpacking as a tuple"""
        return iter(astuple(self))

    def __str__(self):
        if self.aux:
            return f"{self.container}.{self.aux}"
        else:
            return self.container


@dataclass
class Selection:
    #: The dependency
    dependency: Dependency
    #: The number of counts in the selection (None == char)
    counts: Optional[int] = None
    #: Any preselection implicit in this
    preselection: Optional[boolean.Expression] = None
    #: Whether this selection is affected by systematics
    is_sys_varied: bool = False
